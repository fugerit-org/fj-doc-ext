package org.fugerit.java.doc.ext.pdflib;

import java.io.OutputStream;

import org.fugerit.java.doc.base.config.DocInput;
import org.fugerit.java.doc.base.config.DocOutput;
import org.fugerit.java.doc.base.config.DocTypeHandler;
import org.fugerit.java.doc.base.config.DocTypeHandlerDefault;
import org.fugerit.java.doc.base.model.DocBase;

public class PdfLibTypeHandler extends DocTypeHandlerDefault {

	public static DocTypeHandler HANDLER = new PdfLibTypeHandler();
	
	public static final String DOC_OUTPUT_PDF = "pdf";
	
	public PdfLibTypeHandler() {
		super( DOC_OUTPUT_PDF );
	}

	@Override
	public void handle(DocInput docInput, DocOutput docOutput) throws Exception {
		DocBase docBase = docInput.getDoc();
		OutputStream outputStream = docOutput.getOs();
	}

}
